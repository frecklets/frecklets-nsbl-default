#!/usr/bin/env frecklecute

doc:
  short_help: Displays the content of an (internal) Ansible variable.
  examples:
    - title: Display the currents user home directory.
      vars:
        var: ansible_env.HOME

args:
  var:
    type: string
    required: true
    doc:
      short_help: The Ansible variable name to debug.
    cli:
      param_type: argument

meta:
  tags:
    - debug

frecklets:
  - frecklet:
      type: ansible-module
      name: debug
      desc:
        short: "display value of var: {{:: var ::}}"
        references:
          "'debug' Ansible module": "https://docs.ansible.com/ansible/latest/modules/debug_module.html"
      properties:
        idempotent: false
        become: false
        internet: false
    vars:
      var: "{{:: var ::}}"
