doc:
  short_help: Execute an arbitrary Ansible module.
  help: |
    This is a generic task to execute any of the officially supported, [available Ansible modules](https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html).

    Currently only a few basic metadata keys are supported: ``become`` &``become_user``. The other ones will be added
    shortly.
  examples:
    - title: Run the 'debug' module.
      desc: |
        Executes the Ansible [debug](https://docs.ansible.com/ansible/latest/modules/debug_module.html) module, to print an (Ansible-internal) variable value, in this case the username that runs the current playbook. This is useful for debugging.
      vars:
        module_vars:
          var: ansible_env.USER
args:
  name:
    doc:
      short_help: The module name.
    type: string
    required: true
    cli:
      param_type: argument
  become:
    doc:
      short_help: Whether to become another user.
    type: boolean
    required: false
  become_user:
    doc:
      short_help: The user to become.
    type: string
    empty: false
    required: false
  module_vars:
    doc:
      short_help: The parameters for the module.
    type: dict
    required: false
    empty: true
    default: {}

frecklets:
  - frecklet:
      name: ansible-module
      type: ansible-meta
      properties:
        elevated: '{{:: become ::}}'
    task:
      become: "{{:: become ::}}"
      become_user: "{{:: become_user ::}}"
    vars:
      name: "{{:: name ::}}"
      become: "{{:: become ::}}"
      become_user: "{{:: become_user ::}}"
      vars: "{{:: module_vars ::}}"
