doc:
  short_help: Ensure a gitlab runner is registered for a group or project.

args:
  _import: gitlab-runner-service-linux
  install_gitlab_runner:
    doc:
      short_help: Whether to also make sure a gitlab runner service is setup and running.
    type: boolean
    default: false
    cli:
      param_decls:
        - "--install-gitlab-runner"
  gitlab_url:
    doc:
      short_help: The URL of the Gitlab server, with protocol (i.e. http or https).
    type: string
    required: true
    default: "https://gitlab.com"
  api_token:
    doc:
      short_help: Your private token to interact with the GitLab API.
    type: string
    secret: true
    required: true
  registration_token:
    doc:
      short_help: The registration token to use.
    type: string
    required: true
  description:
    doc:
      short_help: The unique name of the runner.
    type: string
    required: true
  tags:
    doc:
      short_help: The tags that apply to the runner.
    type: list
    schema:
      type: string
    required: false
    cli:
      param_decls:
        - "--tag"
        - "-t"
  run_untagged:
    doc:
      short_help: Run untagged jobs or not.
    type: boolean
    required: false
    default: true
  locked:
    doc:
      short_help: Whether the runner is locked or not.
    type: boolean
    default: false
    required: false



frecklets:
  - gitlab-runner-service-linux:
      frecklet::skip: "{{:: install_gitlab_runner | negate ::}}"
      install_docker: "{{:: install_docker ::}}"
      arch: "{{:: arch ::}}"
  - pip-system-package-installed:
      package: python-gitlab
  - frecklet:
      name: gitlab_runner
      type: ansible-module
      properties:
        elevated: false
        internet: true
        idempotent: true
      desc:
        short_help: "register gitlab runner"
    vars:
      url: "{{:: gitlab_url ::}}"
      api_token: "{{:: api_token ::}}"
      registration_token: "{{:: registration_token ::}}"
      description: "{{:: description ::}}"
      state: present
      active: True
      tag_list: "{{:: tags ::}}"
      run_untagged: "{{:: run_untagged ::}}"
      locked: "{{:: locked ::}}"




