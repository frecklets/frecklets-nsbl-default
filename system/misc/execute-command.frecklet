---

doc:
  short_help: Execute a one-off command.
  help: |
    Execute a single command. The command won't be processed through the shell, so it won't reckognize environment
    variables like ``$HOME``, and can't be used to do shell operation like "<", ">", etc.

    Use the frecklet::execute-shell frecklet if that is what you need.

    Behind the scenes, this uses the ['command' Ansible module](https://docs.ansible.com/ansible/latest/modules/command_module.html),
    so check its documentation for more details.
  references:
    "'command' Ansible module": https://docs.ansible.com/ansible/latest/modules/command_module.html
  examples:
    - title: Create a directory via a command.
      desc: |
        Note, usually you'd use the frecklet::folder-exists frecklet for this.
      vars:
        command: "mkdir -p /tmp/parent/folder"
    - title: Create a relative directory via a command.
      desc: |
        Note, usually you'd use the frecklet::folder-exists frecklet for this.
      vars:
        command: "mkdir -p parent/folder"
        chdir: "/tmp"

args:
  command:
    doc:
      short_help: The command to execute.
    type: string
    required: true
  chdir:
    doc:
      short_help: The working directory.
    type: string
    required: false
#  become:
#    doc:
#      short_help: Whether to use elevated privileges when executing the command.
#    type: boolean
#    default: false
  become_user:
    doc:
      short_help: The user to execute this command as.
    type: string
    empty: false
    required: false
  ignore_error:
    doc:
      short_help: Whether to ignore any potential errors.
    type: boolean
    required: false
    default: false
  no_log:
    doc:
      short_help: Whether to hide the log of this command (because for example the command contains sensitive information).
    type: boolean
    default: false
    required: false
  register_target:
    doc:
      short_help: Name of the register to store the result of this command.
    type: string
    required: false

frecklets:

  - task:
      become: "{{:: become_user | true_if_not_empty ::}}"
      become_user: "{{:: become_user ::}}"
      ignore_errors: "{{:: ignore_error ::}}"
      no_log: "{{:: no_log ::}}"
    frecklet:
      name: command
      type: ansible-module
      properties:
        elevated: "{{:: become_user | true_if_not_empty ::}}"
        idempotent: false
      register:
        target: "{{:: register_target ::}}"
        value:
          stdout: "{{ __result__.stdout }}"
          stderr: "{{ __result__.stderr }}"
      desc:
        references:
          "'command' Ansible module": "https://docs.ansible.com/ansible/latest/modules/command_module.html"
        short: "execute one-off command: {{:: command ::}}"
        long: |
          {%:: if chdir ::%}Change the current working directory to be '{{:: chdir ::}}'.{%:: endif ::%}.

          {%:: if become_user ::%}As user '{%:: if become_user ::%}{{:: become_user ::}}{%:: else ::%}'root'{%:: endif ::%} execute {%:: else ::%}Execute{%:: endif ::%}:

              {{:: command ::}}
    vars:
      free_form: "{{:: command ::}}"
      chdir: "{{:: chdir ::}}"

