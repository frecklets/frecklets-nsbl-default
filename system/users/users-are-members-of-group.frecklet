doc:
  short_help: Ensures a list of users are all members of a certain group.
  examples:
    - title: Add user 'freckles' to group 'wheel'.
      vars:
        user: freckles
        group: wheel

args:
  users:
    doc:
      short_help: A list of usernames.
    type: list
    schema:
      type: string
    required: true
    cli:
      param_decls:
        - "--user"
        - "-u"
  group:
    doc:
      short_help: The name of the group.
    type: string
    required: true
    cli:
      param_decls:
        - "--group"
        - "-g"

frecklets:
  - users-exist:
      names: "{{:: users ::}}"
  - group-exists:
      group: "{{:: group ::}}"
  - frecklet:
      name: user
      type: ansible-module
      properties:
        elevated: true
        idempotent: true
        internet: false
      desc:
        short: "Ensure users are members of group '{{:: group ::}}': {{:: users | join(', ') ::}}"
    task:
      become: true
      loop: "{{:: users ::}}"
      loop_control:
        loop_var:
          "__user__"
    vars:
      name: "{{ __user__ }}"
      groups:
        - "{{:: group ::}}"
      append: true

