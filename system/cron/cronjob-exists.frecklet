doc:
  short_help: Ensures a cron job exists.
  help: |
    Ensures a cron job exists.

    This mostly just forwards arguments to the 'cron' Ansible module, please check it's documentation for more details.
  references:
    "'cron' Ansible module": "https://docs.ansible.com/ansible/latest/modules/cron_module.html"

args:
  name:
    doc:
      short_help: Description of a crontab entry.
    type: string
    required: true
  job:
    doc:
      short_help: The command to execute.
    type: string
    required: true
  month:
    doc:
      short_help: "Month of the year the job should run ( 1-12, *, */2, etc )."
    type: string
    required: false
  day:
    doc:
      short_help: "Day of the month the job should run ( 1-31, *, */2, etc )."
    type: string
    required: false
  hour:
    doc:
      short_help: "Hour when the job should run ( 0-23, *, */2, etc )."
    type: string
    required: false
  minute:
    doc:
      short_help: "Minute when the job should run ( 0-59, *, */2, etc )."
    type: string
    required: false
  weekday:
    doc:
      short_help: "Day of the week that the job should run ( 0-6 for Sunday-Saturday, *, etc )."
    type: string
    required: false
  special_time:
    doc:
      short_help: "Special time specification nickname."
    type: string
    required: false
    allowed:
      - "annually"
      - "daily"
      - "hourly"
      - "monthly"
      - "reboot"
      - "weekly"
      - "yearly"
  user:
    doc:
      short_help: "The specific user whose crontab should be modified. When unset, this parameter defaults to using root."
    type: string
    required: false
    default: root

frecklets:
  - frecklet:
      name: cron
      type: ansible-module
      properties:
        idempotent: true
        internet: false
        elevated: true
      desc:
        short: "adding cronjob for '{{:: user ::}}': {{:: job ::}}"
    task:
      become: true
    vars:
      name: "{{:: name ::}}"
      job: "{{:: job ::}}"
      minute: "{{:: minute ::}}"
      hour: "{{:: hour ::}}"
      day: "{{:: day ::}}"
      month: "{{:: month ::}}"
      weekday: "{{:: weekday ::}}"
      special_time: "{{:: special_time ::}}"
      state: present
      user: "{{:: user ::}}"
