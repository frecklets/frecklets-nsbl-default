doc:
  short_help: "Install a Python runtime for a user."
  help: |
    Install a Python environment for a user, using [pyenv](https://github.com/pyenv/pyenv) or system packages.

    If using 'pyenv', this will install all dependencies required to build Python, then it will download the specified version of Python,
    compile it, and use the 'pyenv global' command (which will write that information into the file $HOME/.pyenv/version)
    to make it the default version for the user.

    If Python type is 'pyenv', this will add a piece of code to ``$HOME/.bashrc`` to load pyenv when the user logs in.

  references:
    "pyenv homepage": "https://github.com/pyenv/pyenv"
  examples:
    - title: Install Python version 3.6.8 for user 'freckles', using 'pyenv'.
      vars:
        version: "3.6.8"
        user: freckles
    - title: Install the latest version of Python using 'pyenv'.
    - title: Install system Python, version 2
      vars:
        version: 2
        python_type: system

args:
  version:
    doc:
      short_help: The version of Python to install.
    default: "latest"
    required: true
  user:
    doc:
      short_help: The user to install Python for.
    type: string
    required: false
  path:
    doc:
      short_help: The path to install pyenv into.
      help: |
        Defaults to $HOME/.pyenv
    type: string
    required: false
#  set_global:
#    doc:
#      short_help: Don't set 'global' version.
#    type: boolean
#    required: false
#    default: true
  python_type:
    doc:
      short_help: "How to install Python."
    type: "string"
    required: false
    default: "pyenv"
    allowed:
      - "pyenv"
#      - "conda"
      - "system"
  build_opts:
    doc:
      short_help: Build options to be forwarded (if supported by 'python_type').
    type: string
    required: false
    empty: true

frecklets:
  - user-exists:
      frecklet::skip: "{{:: user | true_if_empty ::}}"
      name: "{{:: user ::}}"
#  - package-installed:
#      package: git
  - frecklet:
      name: freckfrackery.python
      type: ansible-role
      resources:
        ansible-role:
          - freckfrackery.python
          - freckfrackery.install-pkgs
          - freckfrackery.install-pkg-mgrs
          - geerlingguy.homebrew
          - elliotweiser.osx-command-line-tools
      properties:
        idempotent: true
        internet: true
        # elevated: "{%:: if user or python_type == 'system' ::%}true{%:: else ::%}false{%:: endif ::%}"
        elevated: true # because of pyenv dependencies
      desc:
        references:
          "pyenv homepage": "https://github.com/pyenv/pyenv"
    task:
      include-type: include
    vars:
      python_owner: "{{:: user ::}}"
      python_build_opts: "{{:: build_opts ::}}"
      python_type: "{{:: python_type ::}}"
      python_version: "{{:: version ::}}"
      python_tasks: ["install-python"]

