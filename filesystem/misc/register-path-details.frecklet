---

doc:
  short_help: Registers the details of a path (file/folder/link) into a specified register variable.
  help: |
    If the path in question can not be read by the user who executes this frecklet, the 'become' argument needs to be set to 'true'.
  examples:
    - title: Register details of '/home/freckles/.local/share/freckles/bin/freckles' into register variable 'freckles_bin'.
      vars:
        path: /home/freckles/.local/share/freckles/bin/freckles
        register_var: freckles_bin

args:
  path:
    doc:
      short_help: The path to check.
    type: string
    required: true
    cli:
      param_type: argument
  become:
    doc:
      short_help: Whether to use elevated privileges to check the path.
    type: boolean
    default: false
    required: false
    cli:
      param_decls:
        - "--become"
  register_target:
    doc:
      short_help: The name of the variable to register.
    type: string
    required: true

frecklets:
  - frecklet:
      name: stat
      type: ansible-module
      properties:
        elevated: "{{:: become ::}}"
        idempotent: false
        internet: false
      desc:
        short: "getting details for: {{:: path ::}}"
      register: "{{:: register_target ::}}"
    task:
      become: "{{:: become ::}}"
    vars:
      path: "{{:: path ::}}"
