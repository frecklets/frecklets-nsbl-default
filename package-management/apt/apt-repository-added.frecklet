doc:
  short_help: Ensures an apt repository is available and activated.

args:
  repo_url:
    doc:
      short_help: The url of the apt repository.
    type: string
    required: true
    cli:
      param_type: argument
  update_cache:
    doc:
      short_help: Run the equivalent of apt-get update when a change occurs. Cache updates are run after making changes.
    type: boolean
    required: false
    default: true
  repo_alias:
    doc:
      short_help: When provided, the repo will be added in a separate file under '/etc/apt/sources.d', using the value of this argument as filename.
    type: string
    required: false
  codename:
    doc:
      short_help: Override the distribution codename to use for PPA repositories. Should usually only be set when working with a PPA on a non-Ubuntu target (e.g. Debian or Mint).
    type: string
    required: false
  components:
    doc:
      short_help: "List of components for this repo (e.g. ['main', 'non-free']"
    type: list
    schema:
      type: string
    required: false
  is_src_repo:
    doc:
      short_help: Whether the repository should be added into the sources list.
    type: boolean
    default: false
    required: false
    cli:
      param_decls:
        - "--is-src-repo"

frecklets:
  - frecklet:
      name: apt_repository
      type: ansible-module
      properties:
        elevated: true
        idempotent: true
        internet: true
      desc:
        short: "add apt repo: {{:: repo_url ::}}"
    task:
      become: true
    vars:
      repo: "{%:: if not repo_url.startswith('ppa:') ::%}{%:: if is_src_repo is defined and is_src_repo ::%}deb-src {%:: else ::%}deb {%:: endif ::%}{%:: endif ::%}{{:: repo_url ::}}{%:: if codename is defined and codename ::%} {{:: codename ::}}{%:: endif ::%}{%:: if components is defined and components ::%} {{:: components | join(' ') ::}}{%:: endif ::%}"
      update_cache: "{{:: update_cache ::}}"
      state: present
      filename: "{{:: repo_alias ::}}"
      codename: "{{:: codename ::}}"

